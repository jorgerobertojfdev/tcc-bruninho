<?php

use Illuminate\Database\Seeder;

class UserSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create();
        factory(App\User::class)->states('pedagogia')->create();
        factory(App\User::class)->states('psicologia')->create();
        factory(App\User::class)->states('fisioterapia')->create();
        factory(App\User::class)->states('AssistenteSocial')->create();
        factory(App\User::class)->states('FonoAudiologia')->create();
    }
}
