<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => 'Secretaria',
        'login' => 'secretaria',
        'role' => 'secretaria',
        'password' => bcrypt('secretaria'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->state(App\User::class, 'pedagogia', function (Faker $faker) {
    return [
        'name' => 'Pedagogia',
        'login' => 'pedagogia',
        'role' => 'pedagogia',
        'password' => bcrypt('pedagogia'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->state(App\User::class, 'fisioterapia', function (Faker $faker) {
    return [
        'name' => 'Fisioterapia',
        'login' => 'fisioterapia',
        'role' => 'fisioterapia',
        'password' => bcrypt('fisioterapia'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->state(App\User::class, 'psicologia', function (Faker $faker) {
    return [
        'name' => 'Psicologia',
        'login' => 'psicologia',
        'role' => 'psicologia',
        'password' => bcrypt('psicologia'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->state(App\User::class, 'AssistenteSocial', function (Faker $faker) {
    return [
        'name' => 'Assistente Social',
        'login' => 'assistente-social',
        'role' => 'assistente-social',
        'password' => bcrypt('assistente-social'), // secret
        'remember_token' => str_random(10),
    ];
});
$factory->state(App\User::class, 'FonoAudiologia', function (Faker $faker) {
    return [
        'name' => 'FonoAudiologia',
        'login' => 'fonoaudiologia',
        'role' => 'fonoaudiologia',
        'password' => bcrypt('fonoaudiologia'), // secret
        'remember_token' => str_random(10),
    ];
});
