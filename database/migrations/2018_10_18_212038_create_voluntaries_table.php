<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoluntariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voluntaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('conjuge')->nullable();
            $table->string('estado_civil')->nullable();
            $table->string('filhos')->nullable();
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            $table->string('companhia')->nullable();
            $table->string('funcao')->nullable();
            $table->date('data_reuniao')->nullable();
            $table->string('quando_comeca')->nullable();
            $table->string('horario_disponiveis')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voluntaries');
    }
}
