<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialWorkerRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_worker_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('beneficios')->nullable();
            $table->string('condicao_moradia')->nullable();
            $table->decimal('renda_familia', 10, 2)->nullable();
            $table->decimal('renda_percapita', 10, 2)->nullable();

            $table->jsonb('follow_ups')->nullable();

            $table->unsignedInteger('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_worker_records');
    }
}
