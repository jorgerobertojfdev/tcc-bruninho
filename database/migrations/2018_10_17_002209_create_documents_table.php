<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cpf')->nullable();
            $table->string('identidade_numero')->nullable();
            $table->string('identidade_uf')->nullable();
            $table->string('identidade_orgao_expedidor')->nullable();
            $table->date('identidade_data_emissao')->nullable();
            $table->integer('certidao_numero')->nullable();
            $table->string('certidao_cartorio_livro')->nullable();
            $table->string('certidao_folha')->nullable();
            $table->string('certidao_termo')->nullable();
            $table->string('certidao_matricula')->nullable();
            $table->date('certidao_data_emissao')->nullable();
            $table->morphs('documentable');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
