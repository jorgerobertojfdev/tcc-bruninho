@extends('layouts.app')

@section('content')
    <form action="{{route('reports.voluntaries.generate')}}" method="post">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Data de inicio</label>
                    <input type="date" name="start" class="form-control" placeholder="Data de inicio" />
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Data Final</label>
                    <input type="date" name="end" class="form-control" placeholder="Data final" />
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-default" style="margin-top: 31px;">
                    Gerar
                </button>
            </div>
        </div>
    </form>

    @if(isset($results))
        <div class="card">
            <div class="card-body">
                <p>Total de Voluntários: {{count($results)}}</p>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Data de nascimento</th>
                            <th>Função</th>
                            <th>Companhia</th>
                            <th>Quando começa</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($results as $result)
                            <tr>
                                <td>
                                    {{$result->nome}}
                                </td>
                                <td>
                                    {{optiona($result->data_nascimento)->format('d/m/Y')}}
                                </td>
                                <td>
                                    {{$result->funcao}}
                                </td>
                                <td>
                                    {{$result->companhia}}
                                </td>
                                 <td>
                                    {{$result->quando_comeca}}
                                </td>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$results->links()}}
    @endif
@endsection
