@extends('layouts.app')

@section('content')
    <form action="{{route('reports.patients.generate')}}" method="post">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Data de inicio</label>
                    <input type="date" name="start" class="form-control" placeholder="Data de inicio" />
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Data Final</label>
                    <input type="date" name="end" class="form-control" placeholder="Data final" />
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-default" style="margin-top: 31px;">
                    Gerar
                </button>
            </div>
        </div>
    </form>

    @if(isset($results))
        <div class="card">
            <div class="card-body">
                <p>Total de pacientes: {{count($results)}}</p>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Paciente</th>
                            <th>Data de nascimento</th>
                            <th>Total Responsáveis</th>
                            <th>Total de FollowUps Pegadogia</th>
                            <th>Total de FollowUps FonoAudiologia</th>
                            <th>Total de FollowUps Fisioterapia</th>
                            <th>Total de FollowUps Serviço Social</th>
                            <th>Total de FollowUps Psicologia</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($results as $result)
                            <tr>
                                <td>
                                    {{$result->nome}}
                                </td>
                                <td>
                                    {{optional($result->data_nascimento)->format('d/m/Y')}}
                                </td>
                                <td>
                                    {{$result->responsible_count}}
                                </td>
                                <td>
                                    {{isset($result->pedagogyRecord) && count($result->pedagogyRecord->follow_ups)}}
                                </td>
                                 <td>
                                    {{isset($result->phonoaudiologyRecord) && count($result->phonoaudiologyRecord->follow_ups)}}
                                </td>
                                <td>
                                    {{isset($result->physiotherapyRecord) && count($result->physiotherapyRecord->follow_ups)}}
                                </td>
                                <td>
                                    {{isset($result->socialWorkerRecord) && count($result->socialWorkerRecord->follow_ups)}}
                                </td>
                                <td>
                                    {{isset($result->psychologyRecord) && count($result->psychologyRecord->follow_ups)}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$results->links()}}
    @endif
@endsection
