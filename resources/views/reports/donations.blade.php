@extends('layouts.app')

@section('content')
    <form action="{{route('reports.donations.generate')}}" method="post">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Data de inicio</label>
                    <input type="date" name="start" class="form-control" placeholder="Data de inicio" />
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Data Final</label>
                    <input type="date" name="end" class="form-control" placeholder="Data final" />
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-default" style="margin-top: 31px;">
                    Gerar
                </button>
            </div>
        </div>
    </form>

    @if(isset($results))
        <div class="card">
            <div class="card-body">
                <p>Total de Doações: {{count($results)}}</p>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Destino</th>
                            <th>Vencimento</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($results as $result)
                            <tr>
                                <td>
                                    {{$result->descricao}}
                                </td>
                                <td>
                                    {{$result->destino}}
                                </td>
                                <td>
                                    {{optional($result->vencimento)->format('d/m/Y')}}
                                </td>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$results->links()}}
    @endif
@endsection
