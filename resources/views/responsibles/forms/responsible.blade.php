<div class="col-md-12">
    <div class="form-group">
      <label for="exampleFormControlInput1">Paciente</label>
      <select name="responsible[patient_id]" class="form-control" required>
          <option value="">Selecione uma opcao</option>
          @foreach($patients as $patient)
                <option value="{{$patient->id}}"                    @if(
                        (isset($responsible) && ($patient->id === $responsible->patient_id)) ||
                        (old('responsible.patient_id') == $patient->id)
                    )
                    selected
                    @endif>
                    {{ $patient->nome }}
                </option>
          @endforeach
      </select>
    </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nome</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="responsible[nome]"  value="@dataOrEmpty($responsible, 'nome')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de nascimento</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="responsible[data_nascimento]"  value="@dataOrEmpty($responsible, 'data_nascimento', 'Y-m-d')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Parentesco</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="responsible[parentesco]"  value="@dataOrEmpty($responsible, 'parentesco')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Telefone</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="responsible[telefone]"  value="@dataOrEmpty($responsible, 'telefone')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Celular</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="responsible[celular]"  value="@dataOrEmpty($responsible, 'celular')" />
  </div>
</div>
