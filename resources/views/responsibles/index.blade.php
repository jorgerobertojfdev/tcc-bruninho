@extends('layouts.app')

@section('content')
@include('shared.search', [
    'placeholder' => 'Buscar pelo nome',
    'route' => 'responsibles.index',
    'create' => 'responsibles.create',
    'text' => 'Cadastrar novo responsável'
])
<div class="row">
    <div class="col-md-12 card p-5">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Data de nascimento</th>
                    <th>Parentesco</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($responsibles as $responsible)
                    <tr>
                        <td>{{$responsible->id}}</td>
                        <td>{{$responsible->nome}}</td>
                        <td>{{optional($responsible->data_nascimento)->format('d/m/Y')}}</td>
                        <td>{{$responsible->parentesco}}</td>
                        <td>
                            <a href="{{route('responsibles.edit', $responsible->id)}}">Editar</a>
                            @include('shared.delete', ['route' => 'responsibles', 'id' => $responsible->id])
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{$responsibles->links()}}
    </div>
</div>
@endsection

