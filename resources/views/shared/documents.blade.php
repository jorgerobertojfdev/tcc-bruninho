<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Cpf</label>
    <input type="number" class="form-control" id="exampleFormControlInput1" name="document[cpf]" value="@dataOrEmpty($data, 'cpf')" />
  </div>
</div>
<div class="col-md-4">
  <div class="form-group">
    <label for="exampleFormControlInput1">Rg</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[identidade_numero]" value="@dataOrEmpty($data, 'identidade_numero')" />
  </div>
</div>
<div class="col-md-5">
  <div class="form-group">
    <label for="exampleFormControlInput1">Orgão Expedidor</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[identidade_orgao_expedidor]" value="@dataOrEmpty($data, 'identidade_orgao_expedidor')" />
  </div>
</div>

<div class="col-md-2">
  <div class="form-group">
    <label for="exampleFormControlInput1">UF</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[identidade_uf]" value="@dataOrEmpty($data, 'identidade_uf')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de Emissão</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="document[identidade_data_emissao]" value="@dataOrEmpty($data, 'identidade_data_emissao')" />
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label for="exampleFormControlInput1">Certidão</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[certidao_numero]" value="@dataOrEmpty($data, 'certidao_numero')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Cartório Livro</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[certidao_cartorio_livro]" value="@dataOrEmpty($data, 'certidao_cartorio_livro')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Folha</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[certidao_folha]" value="@dataOrEmpty($data, 'certidao_folha')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Termo</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[certidao_termo]" value="@dataOrEmpty($data, 'certidao_termo')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de emissão</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="document[certidao_data_emissao]" value="@dataOrEmpty($data, 'certidao_data_emissao')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Matrícula</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="document[certidao_matricula]" value="@dataOrEmpty($data, 'certidao_matricula')" />
  </div>
</div>
