<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Cep</label>
    <input type="number" @blur="e => getCep(e.target.value)" class="form-control" id="exampleFormControlInput1" name="address[cep]" value="@dataOrEmpty($data, 'cep')" />
  </div>
</div>
<div class="col-md-4">
  <div class="form-group">
    <label for="exampleFormControlInput1">Rua</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" v-model="address.logradouro" name="address[rua]" value="@dataOrEmpty($data, 'rua')" />
  </div>
</div>
<div class="col-md-5">
  <div class="form-group">
    <label for="exampleFormControlInput1">Complemento</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" v-model="address.complemento" v-model="address.complemento" name="address[complemento]" value="@dataOrEmpty($data, 'complemento')" />
  </div>
</div>

<div class="col-md-2">
  <div class="form-group">
    <label for="exampleFormControlInput1">Número</label>
    <input type="number" class="form-control" id="exampleFormControlInput1"  name="address[numero]" value="@dataOrEmpty($data, 'numero')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Bairro</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" v-model="address.bairro" name="address[bairro]" value="@dataOrEmpty($data, 'bairro')" />
  </div>
</div>

<div class="col-md-4">
  <div class="form-group">
    <label for="exampleFormControlInput1">Cidade</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" v-model="address.localidade" name="address[cidade]" value="@dataOrEmpty($data, 'cidade')" />
  </div>
</div>

<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Estado</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" v-model="address.uf" name="address[estado]" value="@dataOrEmpty($data, 'estado')" />
  </div>
</div>
