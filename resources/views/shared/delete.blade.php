<form method="post" action='{{route("{$route}.destroy", $id)}}' onsubmit="if(!confirm('tem certeza que deseja deletar ?')){return false}">
    @csrf
    @method('delete')
    <button class="btn btn-link">Deletar</button>
</form>
