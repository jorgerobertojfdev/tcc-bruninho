<form action="{{route($route)}}">
    <div class="row">
        <div class="col-md-12 card p-3 mb-4">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="{{$placeholder}}" aria-label="{{$placeholder}}" value="{{request('q')}}" aria-describedby="button-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" id="button-addon2">Buscar</button>
              @if(auth()->user()->role === 'secretaria')
                <a href="{{route($create)}}" style="background: navajowhite; color: #000" class="btn btn-outline-primary" type="button" id="button-addon2">
                    {{$text}}
                </a>
              @endif
              </div>
            </div>
        </div>
    </div>
</form>
