@extends('layouts.app')

@section('content')
<div class="card p-3">
  <div class="card-body">
    <h2 class="card-title">Cadastrar Voluntário</h2>
    <hr>
    <div class="row">
      <form action="{{route('voluntaries.store')}}" method="POST">
        @csrf
        @include('shared.errors')
        <div class="col-md-2 mt-4">
          <h3>Dados Pessoais</h3>
        </div>
        <div class="col-md-10 mt-4">
          <div class="row">
            @include('voluntaries.forms.voluntary')
          </div>
        </div>

        <div class="col-md-2 mt-4">
          <h3>Endereço</h3>
        </div>
        <div class="col-md-10 mt-4">
          <div class="row">
            @include('shared.address')
          </div>
        </div>

        <div class="col-md-2 mt-4">
          <h3>Documentos</h3>
        </div>
        <div class="col-md-10 mt-4">
          <div class="row">
            @include('shared.documents')
          </div>
        </div>

        <div class="col-md-12 form-actions mt-4 d-flex justify-content-end">
          <button class="btn btn-primary">Salvar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

