<div class="col-md-12">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nome</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[nome]"  value="@dataOrEmpty($voluntary, 'nome')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de nascimento</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="voluntary[data_nascimento]"  value="@dataOrEmpty($voluntary, 'data_nascimento', 'Y-m-d')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data da reunão</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="voluntary[data_reuniao]"  value="@dataOrEmpty($voluntary, 'data_reuniao')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Cônjuge</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[conjuge]"  value="@dataOrEmpty($voluntary, 'conjuge')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Estado Civíl</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[estado_civil]"  value="@dataOrEmpty($voluntary, 'estado_civil')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Filhos</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[filhos]"  value="@dataOrEmpty($voluntary, 'filhos')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Celular</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[celular]"  value="@dataOrEmpty($voluntary, 'celular')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Telefone</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[telefone]"  value="@dataOrEmpty($voluntary, 'telefone')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Com quem vai trabalhar</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[companhia]"  value="@dataOrEmpty($voluntary, 'companhia')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Função</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[funcao]"  value="@dataOrEmpty($voluntary, 'funcao')" />
  </div>
</div><div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Quando Começa</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[quando_comeca]"  value="@dataOrEmpty($voluntary, 'quando_comeca')" />
  </div>
</div><div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Horario disponíveis</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="voluntary[horario_disponiveis]"  value="@dataOrEmpty($voluntary, 'horario_disponiveis')" />
  </div>
</div>
