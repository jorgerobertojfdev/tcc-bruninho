@extends('layouts.app')

@section('content')
@include('shared.search', [
    'placeholder' => 'Buscar pelo nome',
    'route' => 'voluntaries.index',
    'create' => 'voluntaries.create',
    'text' => 'Cadastrar novo voluntário'
])
<div class="row">
    <div class="col-md-12 card p-5">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Data de nascimento</th>
                    <th>Função</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($voluntaries as $voluntary)
                <tr>
                    <td>{{$voluntary->id}}</td>
                    <td>{{$voluntary->nome}}</td>
                    <td>{{$voluntary->data_nascimento->format('d/m/Y')}}</td>
                    <td>{{$voluntary->funcao}}</td>
                    <td>
                        <a href="{{route('voluntaries.edit', $voluntary->id)}}">Editar</a>
                        @include('shared.delete', ['route' => 'voluntaries', 'id' => $voluntary->id])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$voluntaries->links()}}
    </div>
</div>
@endsection

