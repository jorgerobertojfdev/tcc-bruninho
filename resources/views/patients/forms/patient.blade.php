<div class="col-md-12">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nome do atendido</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[nome]"  value="@dataOrEmpty($patient, 'nome')" value="@dataOrEmpty($patient, 'nome')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de nascimento</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="patient[data_nascimento]"  value="@dataOrEmpty($patient, 'data_nascimento')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Sexo</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[sexo]"  value="@dataOrEmpty($patient, 'sexo')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Naturalidade</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[naturalidade]"  value="@dataOrEmpty($patient, 'naturalidade')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Estado Civil</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[estado_civil]"  value="@dataOrEmpty($patient, 'estado_civil')" />
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nome Mãe</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[nome_mae]"  value="@dataOrEmpty($patient, 'nome_mae')" />
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nome Pai</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[nome_pai]"  value="@dataOrEmpty($patient, 'nome_pai')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nis</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[nis]"  value="@dataOrEmpty($patient, 'nis')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Etilogia</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[etilogia]"  value="@dataOrEmpty($patient, 'etilogia')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Deficiência</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[deficiencia]"  value="@dataOrEmpty($patient, 'deficiencia')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Medicação</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="patient[medicacao]"  value="@dataOrEmpty($patient, 'medicacao')" />
  </div>
</div>
