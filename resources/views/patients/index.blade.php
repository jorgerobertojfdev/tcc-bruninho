@extends('layouts.app')

@section('content')
@include('shared.search', [
    'placeholder' => 'Buscar pelo nome',
    'route' => 'patients.index',
    'create' => 'patients.create',
    'text' => 'Cadastrar novo paciente',
    'showButtonCreate' => in_array(auth()->user()->role, ['secretaria'])
])
<div class="row">
    <div class="col-md-12 card p-5">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Deficiência</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($patients as $patient)
                    <tr>
                        <td>{{$patient->id}}</td>
                        <td>{{$patient->nome}}</td>
                        <td>{{$patient->deficiencia}}</td>
                        <td>
                            @if(in_array(auth()->user()->role, ['secretaria']))
                                <a href="{{route('patients.edit', $patient->id)}}">Editar</a>
                                @include('shared.delete', ['route' => 'patients', 'id' => $patient->id])
                            @else
                                <a href="{{route('patients.followups.create', $patient->id)}}">Cadastrar Follow Up</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{$patients->links()}}
    </div>
</div>
@endsection

