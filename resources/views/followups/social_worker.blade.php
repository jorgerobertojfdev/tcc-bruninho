<table class="table table-bordered">
  <tr>
    <td>Nome do atendido</td>
    <td>{{$patient->nome}}</td>
  </tr>
  <tr>
    <td>Renda Familiar</td>
    <td><input type="number" name="followup[renda_familia]" class="form-control" value="{{optional($data)->renda_familia}}"/></td>
  </tr>
  <tr>
    <td>Renda Per Capita</td>
    <td><input type="text" name="followup[renda_percapita]" class="form-control" value="{{optional($data)->renda_percapita}}"/></td>
  </tr>
  <tr>
    <td>Beneficios</td>
    <td><input type="text" name="followup[beneficios]" class="form-control" value="{{optional($data)->beneficios}}"/></td>
  </tr>
  <tr>
    <td>Condições de moradia</td>
    <td><input type="text" name="followup[condicao_moradia]" class="form-control" value="{{optional($data)->condicao_moradia}}"/></td>
  </tr>
</table>
