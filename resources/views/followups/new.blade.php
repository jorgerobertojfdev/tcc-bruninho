@extends('layouts.app')

@section('content')
<div class="card p-3">
  <div class="card-body">
    <h2 class="card-title">Adicionar FollowUp</h2>
    <hr>
    <form action="{{ $idRecord ? route('patients.followups.update', [$patient->id, $idRecord]) : route('patients.followups.store', $patient->id)}}" method="POST">
        @csrf
        @method($idRecord ? 'put' : 'post')

        @is('pedagogia')
            @include('followups.pedagogy', ['data' => $relation])
        @elseis('assistente-social')
            @include('followups.social_worker', ['data' => $relation])
        @else
            @include('followups.other', ['data' => $relation])
        @endis
        <h4>FollowUps</h4>
        <div class="form-group">
            <label for="date">Data</label>
            <input type="date" name="follow_ups[0][date]" class="form-control" />
        </div>
        <div class="form-group">
            <label for="date">Follow up</label>
            <textarea name="follow_ups[0][text]" class="form-control" id="" cols="30" rows="3"></textarea>
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>FollowUp</th>
                </tr>
            </thead>
            <tbody>
                @if($relation)
                    @foreach($relation->follow_ups as $followUp)
                    <tr>
                        <td>{{$followUp->date}}</td>
                        <td>{{$followUp->text}}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

        <div class="col-md-12 form-actions mt-4 d-flex justify-content-end">
          <button class="btn btn-primary">Salvar</button>
        </div>
      </form>
  </div>
</div>
@endsection
