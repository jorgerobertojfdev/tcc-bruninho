<table class="table table-bordered">
  <tr>
    <td>Nome do atendido</td>
    <td>{{$patient->nome}}</td>
  </tr>
  <tr>
    <td>Data de nascimento</td>
    <td>{{$patient->data_nascimento->format('d/m/Y')}}</td>
  </tr>
  <tr>
    <td>Deficiência</td>
    <td>{{$patient->deficiencia}}</td>
  </tr>
  <tr>
    <td>Prognóstico</td>
    <td><input type="text" name="followup[prognostico]" class="form-control" value="{{optional($data)->prognostico}}"/></td>
  </tr>
  <tr>
    <td>Diagnóstico</td>
    <td><input type="text" name="followup[diagnostico]" class="form-control" value="{{optional($data)->diagnostico}}"/></td>
  </tr>
  <tr>
    <td>Plano de tratamento</td>
    <td><input type="text" name="followup[plano_tratamento]" class="form-control" value="{{optional($data)->plano_tratamento}}"/></td>
  </tr>
</table>
