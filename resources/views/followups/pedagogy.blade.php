<table class="table table-bordered">
          <tr>
            <td>Nome do atendido</td>
            <td>{{$patient->nome}}</td>
          </tr>
          <tr>
            <td>Data de nascimento</td>
            <td>{{$patient->data_nascimento->format('d/m/Y')}}</td>
          </tr>
          <tr>
            <td>Deficiência</td>
            <td>{{$patient->deficiencia}}</td>
          </tr>
          <tr>
            <td>Etilogia</td>
            <td>{{$patient->etilogia}}</td>
          </tr>
          <tr>
            <td>Responsável</td>
            <td>{{optional($patient->responsible)->nome}}</td>
          </tr>
          <tr>
            <td>Alteração Sensorial</td>
            <td><input type="text" name="followup[alteracao_sensorial]" class="form-control" value="{{optional($patient->pedagogyRecord)->alteracao_sensorial}}"/></td>
          </tr>
          <tr>
            <td>Alteração Comportamental</td>
            <td><input type="text" name="followup[alteracao_comportamental]" class="form-control" value="{{optional($patient->pedagogyRecord)->alteracao_comportamental}}"/></td>
          </tr>
          <tr>
            <td>Medicação</td>
            <td>{{$patient->mediacao}}</td>
          </tr>
          <tr>
            <td>Escola</td>
            <td><input type="text" name="followup[escola]" class="form-control" value="{{optional($patient->pedagogyRecord)->escola}}"/></td>
          </tr>
        </table>
