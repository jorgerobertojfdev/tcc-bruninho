<div class="col-md-12">
    <div class="form-group">
      <label for="exampleFormControlInput1">Paciente</label>
      {{old('member.patient_id')}}
      <select name="member[patient_id]" class="form-control" required>
          <option value="">Selecione uma opcao</option>
          @foreach($patients as $patient)
                <option value="{{$patient->id}}"
                    @if(
                        (isset($member) && ($patient->id === $member->patient_id)) ||
                        (old('member.patient_id') == $patient->id)
                    )
                    selected
                    @endif>
                    {{ $patient->nome }}
                </option>
          @endforeach
      </select>
    </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nome</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="member[nome]"  value="@dataOrEmpty($member, 'nome')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de nascimento</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="member[data_nascimento]"  value="@dataOrEmpty($member, 'data_nascimento', 'Y-m-d')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Parentesco</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="member[parentesco]"  value="@dataOrEmpty($member, 'parentesco')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Telefone</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="member[telefone]"  value="@dataOrEmpty($member, 'telefone')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Celular</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="member[celular]"  value="@dataOrEmpty($member, 'celular')" />
  </div>
</div>
