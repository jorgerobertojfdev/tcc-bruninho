@extends('layouts.app')

@section('content')
@include('shared.search', [
    'placeholder' => 'Buscar pelo nome',
    'route' => 'members.index',
    'create' => 'members.create',
    'text' => 'Cadastrar nova doação'
])

<div class="row">
    <div class="col-md-12 card p-5">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Data de nascimento</th>
                    <th>Parentesco</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($members as $member)
                    <tr>
                        <td>{{$member->id}}</td>
                        <td>{{$member->nome}}</td>
                        <td>{{optional($member->data_nascimento)->format('d/m/Y')}}</td>
                        <td>{{$member->parentesco}}</td>
                        <td>
                            <a href="{{route('members.edit', $member->id)}}">Editar</a>
                            @include('shared.delete', ['route' => 'members', 'id' => $member->id])
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{$members->links()}}
    </div>
</div>
@endsection

