@extends('layouts.app')

@section('content')
@include('shared.search', [
    'placeholder' => 'Buscar pela descrição',
    'route' => 'donations.index',
    'text' => 'Cadastrar nova doação',
    'create' => 'donations.create',
])
<div class="row">
    <div class="col-md-12 card p-5">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Descrição</th>
                    <th>Destino</th>
                    <th>Entrada</th>
                    <th>Vencimento</th>
                    <th>Saida</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($donations as $donation)
                <tr>
                    <td>{{$donation->id}}</td>
                    <td>{{$donation->descricao}}</td>
                    <td>{{$donation->destino}}</td>
                    <td>{{$donation->data_entrada->format('d/m/Y')}}</td>
                    <td>{{$donation->vencimento->format('d/m/Y')}}</td>
                    <td>{{$donation->data_saida->format('d/m/Y')}}</td>
                    <td>
                        <a href="{{route('donations.edit', $donation->id)}}">Editar</a>
                        @include('shared.delete', ['route' => 'donations', 'id' => $donation->id])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$donations->appends(['q' => request('q')])->links()}}
    </div>
</div>
@endsection

