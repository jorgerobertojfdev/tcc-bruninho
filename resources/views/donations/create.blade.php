@extends('layouts.app')

@section('content')
<div class="card p-3">
  <div class="card-body">
    <h2 class="card-title">Cadastrar Doação</h2>
    <hr>
    <div class="row">
      <form action="{{route('donations.store')}}" method="POST">
        @csrf
        @include('shared.errors')
        <div class="row">
            @include('donations.forms.donation')
        </div>

        <div class="col-md-12 form-actions mt-4 d-flex justify-content-end">
          <button class="btn btn-primary">Salvar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

