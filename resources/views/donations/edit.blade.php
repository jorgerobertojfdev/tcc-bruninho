@extends('layouts.app')

@section('content')
<div class="card p-3">
  <div class="card-body">
    <h2 class="card-title">Editar Doação</h2>
    <hr>
    <div class="row">
      <form action="{{route('donations.update', $donation->id)}}" method="POST">
        @csrf
        @method('PUT')
        @include('shared.errors')
        <div class="row">
            @include('donations.forms.donation', ['donation' => $donation])
        </div>

        <div class="col-md-12 form-actions mt-4 d-flex justify-content-end">
          <button class="btn btn-primary">Salvar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

