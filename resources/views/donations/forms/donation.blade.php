<div class="col-md-12">
  <div class="form-group">
    <label for="exampleFormControlInput1">Descrição</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="donation[descricao]"  value="@dataOrEmpty($donation, 'descricao')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Destino</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="donation[destino]"  value="@dataOrEmpty($donation, 'destino')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de entrada</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="donation[data_entrada]"  value="@dataOrEmpty($donation, 'data_entrada', 'Y-m-d')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de vencimento</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="donation[vencimento]"  value="@dataOrEmpty($donation, 'vencimento', 'Y-m-d')" />
  </div>
</div>
<div class="col-md-3">
  <div class="form-group">
    <label for="exampleFormControlInput1">Data de Saida</label>
    <input type="date" class="form-control" id="exampleFormControlInput1" name="donation[data_saida]"  value="@dataOrEmpty($donation, 'data_saida', 'Y-m-d')" />
  </div>
</div>
