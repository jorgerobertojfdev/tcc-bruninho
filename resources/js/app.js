
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    data() {
        return {
            address: {}
        };
    },
    methods: {
        async getCep(cep) {
            if (!cep && cep.length < 8) {
                return false;
            }

            const result = await axios.get(`https://viacep.com.br/ws/${cep}/json/`);
            this.address = result.data;
        }
    }
});
