<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware(['auth'])->group(function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('patients', 'PatientsController');
    Route::resource('patients.followups', 'FollowUpsController');
    Route::resource('members', 'MembersController');
    Route::resource('responsibles', 'ResponsiblesController');
    Route::resource('voluntaries', 'VoluntariesController');
    Route::resource('donations', 'DonationsController');

    Route::get('/reports/patients', 'ReportsController@patients')->name('reports.patients');
    Route::post('/reports/patients', 'ReportsController@patientsGenerate')->name('reports.patients.generate');

    Route::get('/reports/voluntaries', 'ReportsController@voluntaries')->name('reports.voluntaries');
    Route::post('/reports/voluntaries', 'ReportsController@voluntariesGenerate')->name('reports.voluntaries.generate');

    Route::get('/reports/donations', 'ReportsController@donations')->name('reports.donations');
    Route::post('/reports/donations', 'ReportsController@donationsGenerate')->name('reports.donations.generate');
});
