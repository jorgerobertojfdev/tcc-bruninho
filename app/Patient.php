<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
        'nome',
        'data_nascimento',
        'sexo',
        'naturalidade',
        'estado_civil',
        'nome_mae',
        'nome_pai',
        'nis',
        'etilogia',
        'deficiencia',
        'medicacao',
        'especialidade'
    ];

    protected $casts = [
        'data_nascimento' => 'date:Y-m-d',
        'identidade_data_emissao' => 'date:Y-m-d',
        'certidao_data_emissao' => 'date:Y-m-d'
    ];

    public function pedagogyRecord() {
        return $this->hasOne('\App\PedagogyRecord');
    }

    public function phonoaudiologyRecord() {
        return $this->hasOne('\App\PhonoaudiologyRecord');
    }

    public function physiotherapyRecord() {
        return $this->hasOne('\App\PhysiotherapyRecord');
    }

    public function socialWorkerRecord() {
        return $this->hasOne('\App\SocialWorkerRecord');
    }

    public function psychologyRecord() {
        return $this->hasOne('\App\PsychologyRecord');
    }

    public function responsible() {
        return $this->hasOne('App\Responsible');
    }

    public function address() {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function document() {
        return $this->morphOne('App\Document', 'documentable');
    }

    public function getIdentidadeDataEmissaoAttribute() {
        return $this->attributes['identidade_data_emissao'];
    }

    public function getCertidaoDataEmissaoAttribute() {
        return $this->attributes['certidao_data_emissao'];
    }
}
