<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'nome',
        'parentesco',
        'data_nascimento',
        'telefone',
        'celular',
        'patient_id'
    ];
    protected $dates = ['data_nascimento'];

    protected $casts = [
        'data_nascimento' => 'date:Y-m-d',
    ];

    public function patient() {
        return $this->belongsTo('App\Patient');
    }

    public function address() {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function document() {
        return $this->morphOne('App\Document', 'documentable');
    }
}
