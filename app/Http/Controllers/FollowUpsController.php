<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Patient;
use \App\PedagogyRecord;
use Illuminate\Support\Str;

class FollowUpsController extends Controller
{
    private $mappings = [
        'pedagogia' => 'pedagogyRecord',
        'fonoaudiologia' => 'phonoaudiologyRecord',
        'fisioterapia' => 'physiotherapyRecord',
        'assistente-social' => 'socialWorkerRecord',
        'psicologia' => 'psychologyRecord'
    ];

    public function create($id) {
        $patient = Patient::with(['responsible', $this->getRelationByRole() => function ($query) {
            $query->where('user_id', auth()->user()->id);
        }])->find($id);

        $idRecord = optional($patient->{$this->getRelationByRole()})->id;
        $relation = $patient->{$this->getRelationByRole()};

        return view('followups.new', compact('patient', 'idRecord', 'relation'));
    }

    public function store($id, Request $request) {
        $data = array_merge(request('followup'), ['patient_id' => $id]);
        $data['follow_ups'] = collect(request('follow_ups'))->reject(function ($item) {
            return is_null($item['date']) && is_null($item['text']);
        })->map(function ($item) {
            return array_merge($item, ['id' => Str::uuid()]);
        });

        auth()->user()->{$this->getRelationByRole()}()->create($data);

        return redirect()->back();
    }


    public function update($id, $idRecord, Request $request) {
        $user = auth()->user();

        $pedagogy = $user->{$this->getRelationByRole()}()->find($idRecord);

        $data = array_merge(request('followup'), ['patient_id' => $id]);
        $data['follow_ups'] = collect(request('follow_ups'))->reject(function ($item) {
            return is_null($item['date']) && is_null($item['text']);
        })->map(function ($item) {
            return array_merge($item, ['id' => Str::uuid()]);
        })->merge($pedagogy->follow_ups)->all();

       //dd($data);

        $pedagogy->update($data);

        return redirect()->back();
    }

    private function getRelationByRole() {
        $user = auth()->user();

        return $this->mappings[$user->role];
    }
}
