<?php

namespace App\Http\Controllers;

use App\Voluntary;
use Illuminate\Http\Request;

class VoluntariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voluntaries = $this->search(Voluntary::query(), [
            'nome' => request('q', null),
        ]);

        return view('voluntaries.index', compact('voluntaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('voluntaries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Voluntary $voluntary)
    {
       request()->validate([
            'voluntary.nome' => 'required',
            'voluntary.data_nascimento' => 'required',
            'voluntary.conjuge' => 'required',
            'voluntary.telefone' => 'required',
            'document.identidade_numero' => 'required',
            'document.identidade_uf' => 'required',
            'document.cpf' => 'required|cpf',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
        ],
        [
            'voluntary.nome.required' => 'O Campo Nome é obrigatório.',
            'voluntary.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'voluntary.conjuge.required' => 'O Campo Conjuge é obrigatório.',
            'voluntary.telefone.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_numero.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_uf.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.required' => 'O Campo Cpf é obrigatório.',
            'document.cpf.cpf' => 'O Campo Cpf não é um cpf válido.'
        ]);

        $createdVoluntary = $voluntary->create(request('voluntary'));

        $createdVoluntary->address()->save(new \App\Address(request('address')));
        $createdVoluntary->document()->create(request('document'));

        return redirect()->route('voluntaries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voluntary  $voluntary
     * @return \Illuminate\Http\Response
     */
    public function show(Voluntary $voluntary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voluntary  $voluntary
     * @return \Illuminate\Http\Response
     */
    public function edit(Voluntary $voluntary)
    {
        return view('voluntaries.edit', compact('voluntary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voluntary  $voluntary
     * @return \Illuminate\Http\Response
     */
    public function update(Voluntary $voluntary, Request $request)
    {
       request()->validate([
            'voluntary.nome' => 'required',
            'voluntary.data_nascimento' => 'required',
            'voluntary.conjuge' => 'required',
            'voluntary.telefone' => 'required',
            'document.identidade_numero' => 'required',
            'document.identidade_uf' => 'required',
            'document.cpf' => 'required|cpf',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
        ],
        [
            'voluntary.nome.required' => 'O Campo Nome é obrigatório.',
            'voluntary.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'voluntary.conjuge.required' => 'O Campo Conjuge é obrigatório.',
            'voluntary.telefone.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_numero.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_uf.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.required' => 'O Campo Cpf é obrigatório.',
            'document.cpf.cpf' => 'O Campo Cpf não é um cpf válido.'
        ]);

        $voluntary->update(request('voluntary'));

        $voluntary->address()->update(request('address'));
        $voluntary->document()->update(request('document'));

        return redirect()->route('voluntaries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voluntary  $voluntary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voluntary $voluntary)
    {
        $voluntary->delete();

        return redirect()->route('voluntaries.index');
    }
}
