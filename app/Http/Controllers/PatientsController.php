<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;

class PatientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = $this->search(Patient::query(), [
            'nome' => request('q', null),
        ]);
        return view('patients.index', compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        request()->validate([
            'patient.nome' => 'required',
            'patient.data_nascimento' => 'required',
            'patient.etilogia' => 'required',
            'patient.deficiencia' => 'required',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
            'document.cpf' => 'nullable|cpf'
        ],
        [
            'patient.nome.required' => 'O Campo Nome é obrigatório.',
            'patient.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'patient.etilogia.required' => 'O Campo etilogia é obrigatório.',
            'patient.deficiencia.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.cpf' => 'O Campo cpf não é um cpf válido.'
        ]);

        $createdPatient = $patient->create(request('patient'));

        $createdPatient->address()->save(new \App\Address(request('address')));
        $createdPatient->document()->create(request('document'));

        return redirect()->route('patients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        return view('patients.edit', compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Patient $patient, Request $request)
    {
        request()->validate([
            'patient.nome' => 'required',
            'patient.data_nascimento' => 'required',
            'patient.etilogia' => 'required',
            'patient.deficiencia' => 'required',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
            'document.cpf' => 'nullable|cpf'
        ],
        [
            'patient.nome.required' => 'O Campo Nome é obrigatório.',
            'patient.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'patient.etilogia.required' => 'O Campo etilogia é obrigatório.',
            'patient.deficiencia.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.cpf' => 'O Campo Cpf não é um cpf válido.'
        ]);

        $patient->update(request('patient'));

        $patient->address()->update(request('address'));
        $patient->document()->update(request('document'));

        return redirect()->route('patients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        $patient->delete();

        return redirect()->route('patients.index');
    }
}
