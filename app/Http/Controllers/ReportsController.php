<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function patients() {
        return view('reports.patients');
    }

    public function patientsGenerate() {
        $data = array_filter(request(['start', 'end']));
        $results = null;

        if (count($data) > 0) {
            $results = \App\Patient::withCount([
                'responsible',
            ])
            ->with([
                'pedagogyRecord',
                'phonoaudiologyRecord',
                'physiotherapyRecord',
                'socialWorkerRecord',
                'psychologyRecord'
            ])->whereBetween('created_at', array_values($data))->paginate(10);
        }

        return view('reports.patients', compact('results'));
    }

    public function voluntaries() {
        return view('reports.voluntaries');
    }

    public function voluntariesGenerate() {
        $data = array_filter(request(['start', 'end']));
        $results = null;

        if (count($data) > 0) {
            $results = \App\Voluntary::whereBetween('created_at', array_values($data))->paginate(10);
        }

        return view('reports.voluntaries', compact('results'));
    }

    public function donations() {
        return view('reports.donations');
    }

    public function donationsGenerate() {
        $data = array_filter(request(['start', 'end']));
        $results = null;

        if (count($data) > 0) {
            $results = \App\Donation::whereBetween('created_at', array_values($data))->paginate(10);
        }

        return view('reports.donations', compact('results'));
    }
}
