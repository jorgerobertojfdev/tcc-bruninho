<?php

namespace App\Http\Controllers;

use App\Responsible;
use App\Patient;
use Illuminate\Http\Request;

class ResponsiblesController extends Controller
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responsibles = $this->search(Responsible::query(), [
            'nome' => request('q', null),
        ]);

        return view('responsibles.index', compact('responsibles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $patients = Patient::all();
        return view('responsibles.create', compact('patients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Responsible $responsible)
    {
        request()->validate([
            'responsible.nome' => 'required',
            'responsible.data_nascimento' => 'required',
            'responsible.parentesco' => 'required',
            'responsible.telefone' => 'required',
            'document.identidade_numero' => 'required',
            'document.identidade_uf' => 'required',
            'document.cpf' => 'required|cpf',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
        ],
        [
            'responsible.nome.required' => 'O Campo Nome é obrigatório.',
            'responsible.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'responsible.parentesco.required' => 'O Campo Parentesco é obrigatório.',
            'responsible.telefone.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_numero.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_uf.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.required' => 'O Campo cpf é obrigatório.',
            'document.cpf.cpf' => 'O Campo cpf não é um cpf válido.'
        ]);

        $createdResponsible = $responsible->create(request('responsible'));

        $createdResponsible->address()->save(new \App\Address(request('address')));
        $createdResponsible->document()->create(request('document'));

        return redirect()->route('responsibles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function show(Responsible $responsible)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function edit(Responsible $responsible)
    {
        $patients = Patient::all();
        return view('responsibles.edit', compact('responsible', 'patients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function update(Responsible $responsible, Request $request)
    {
        request()->validate([
            'responsible.nome' => 'required',
            'responsible.data_nascimento' => 'required',
            'responsible.parentesco' => 'required',
            'responsible.telefone' => 'required',
            'document.identidade_numero' => 'required',
            'document.identidade_uf' => 'required',
            'document.cpf' => 'required|cpf',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
        ],
        [
            'responsible.nome.required' => 'O Campo Nome é obrigatório.',
            'responsible.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'responsible.parentesco.required' => 'O Campo Parentesco é obrigatório.',
            'responsible.telefone.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_numero.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_uf.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.required' => 'O Campo Cpf é obrigatório.',
            'document.cpf.cpf' => 'O Campo Cpf não é um cpf válido.'
        ]);

        $responsible->update(request('responsible'));

        $responsible->address()->update(request('address'));
        $responsible->document()->update(request('document'));

        return redirect()->route('responsibles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function destroy(Responsible $responsible)
    {
        $responsible->delete();

        return redirect()->route('responsibles.index');
    }
}
