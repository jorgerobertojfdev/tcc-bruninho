<?php

namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;

trait Searchable {
    function search(Builder $model, array $where = []) {
        $conditions = array_filter($where);

        if(count($conditions) > 0) {
            return $model->where(function($query) use ($conditions) {
                foreach ($conditions as $key => $value) {
                    $query->where($key, 'like', "%{$value}%");
                }
            })->paginate(10);
        }

        return $model->paginate(10);
    }
}
