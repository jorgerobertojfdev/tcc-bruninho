<?php

namespace App\Http\Controllers;

use App\Member;
use App\Patient;
use Illuminate\Http\Request;

class MembersController extends Controller
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = $this->search(Member::query(), [
            'nome' => request('q', null)
        ]);
        return view('members.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $patients = Patient::all();
        return view('members.create', compact('patients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Member $member)
    {
        request()->validate([
            'member.nome' => 'required',
            'member.data_nascimento' => 'required',
            'member.parentesco' => 'required',
            'member.telefone' => 'required',
            'document.identidade_numero' => 'required',
            'document.identidade_uf' => 'required',
            'document.cpf' => 'required|cpf',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
        ],
        [
            'member.nome.required' => 'O Campo Nome é obrigatório.',
            'member.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'member.parentesco.required' => 'O Campo Parentesco é obrigatório.',
            'member.telefone.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_numero.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_uf.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.required' => 'O Campo cpf é obrigatório.',
            'document.cpf.cpf' => 'O Campo cpf não é um cpf válido.'
        ]);
        $createdMember = $member->create(request('member'));

        $createdMember->address()->save(new \App\Address(request('address')));
        $createdMember->document()->create(request('document'));

        return redirect()->route('members.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        $patients = Patient::all();
        return view('members.edit', compact('member', 'patients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Member $member, Request $request)
    {
        request()->validate([
            'member.nome' => 'required',
            'member.data_nascimento' => 'required',
            'member.parentesco' => 'required',
            'member.telefone' => 'required',
            'document.identidade_numero' => 'required',
            'document.identidade_uf' => 'required',
            'document.cpf' => 'required|cpf',
            'address.cep' => 'required',
            'address.rua' => 'required',
            'address.numero' => 'required',
            'address.bairro' => 'required',
            'address.cidade' => 'required',
            'address.estado' => 'required',
        ],
        [
            'member.nome.required' => 'O Campo Nome é obrigatório.',
            'member.data_nascimento.required' => 'O Campo Data de nascimento é obrigatório.',
            'member.parentesco.required' => 'O Campo Parentesco é obrigatório.',
            'member.telefone.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_numero.required' => 'O Campo Telefone é obrigatório.',
            'document.identidade_uf.required' => 'O Campo Deficiência é obrigatório.',
            'address.cep.required' => 'O Campo Cep é obrigatório.',
            'address.rua.required' => 'O Campo Rua é obrigatório.',
            'address.numero.required' => 'O Campo Número é obrigatório.',
            'address.bairro.required' => 'O Campo bairro é obrigatório.',
            'address.cidade.required' => 'O Campo Cidade é obrigatório.',
            'address.estado.required' => 'O Campo Bairro é obrigatório.',
            'document.cpf.required' => 'O Campo Cpf é obrigatório.',
            'document.cpf.cpf' => 'O Campo Cpf não é um cpf válido.'
        ]);
        $member->update(request('member'));

        $member->address()->update(request('address'));
        $member->document()->update(request('document'));

        return redirect()->route('members.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();

        return redirect()->route('members.index');
    }
}
