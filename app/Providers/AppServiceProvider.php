<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        \Blade::if('is', function ($role) {
            return auth()->check() && auth()->user()->role === $role;
        });
        \Blade::directive('dataOrEmpty', function ($expression) {
            list($model, $field, $dateFormat) = array_pad(explode(', ', preg_replace('/\'/', '', $expression)), 3, null);

            $isRelationship = strpos($field, '->') !== false;

            if ($isRelationship) {
                list($relation, $fieldRelation) = explode('->', trim($field, "'"));

                $newField = !is_null($dateFormat) ? "optional({$fieldRelation})->format({$dateFormat})" : $fieldRelation;

                return "<?php echo isset({$model}) ? {$model}->{$relation}->{$newField} : '' ?>";
            }

            $newSimpleField = !is_null($dateFormat) ? "{$field}->format('{$dateFormat}')" : $field;
            return "<?php echo isset({$model}) && {$model}->{$field} ? {$model}->{$newSimpleField} : '' ?>";
        });

       // \Route::model('paciente', \App\Patient::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
