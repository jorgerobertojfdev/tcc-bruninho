<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhysiotherapyRecord extends Model
{
    protected $fillable = [
        'plano_tratamento',
        'diagnostico',
        'prognostico',
        'follow_ups',
        'patient_id'
    ];

    protected $casts = [
        'follow_ups' => 'object',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function patient() {
        return $this->belongsTo('App\Patient');
    }
}
