<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialWorkerRecord extends Model
{
    protected $fillable = [
        'beneficios',
        'condicao_moradia',
        'renda_familia',
        'renda_percapita',
        'follow_ups',
        'patient_id'
    ];

    protected $casts = [
        'follow_ups' => 'object',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function patient() {
        return $this->belongsTo('App\Patient');
    }
}
