<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pedagogyRecord() {
        return $this->hasOne('\App\PedagogyRecord');
    }

    public function phonoaudiologyRecord() {
        return $this->hasOne('\App\PhonoaudiologyRecord');
    }

    public function physiotherapyRecord() {
        return $this->hasOne('\App\PhysiotherapyRecord');
    }

    public function socialWorkerRecord() {
        return $this->hasOne('\App\SocialWorkerRecord');
    }

    public function psychologyRecord() {
        return $this->hasOne('\App\PsychologyRecord');
    }

}
