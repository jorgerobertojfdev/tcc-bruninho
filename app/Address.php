<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'id',
        'rua',
        'cep',
        'complemento',
        'cidade',
        'numero',
        'bairro',
        'estado'
    ];

    public function addressable()
    {
        return $this->morphTo();
    }
}
