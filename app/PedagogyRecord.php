<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedagogyRecord extends Model
{
    protected $fillable = [
        'alteracao_sensorial',
        'alteracao_comportamental',
        'follow_ups',
        'escola',
        'patient_id'
    ];

    protected $casts = [
        'follow_ups' => 'object',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function patient() {
        return $this->belongsTo('App\Patient');
    }
}
