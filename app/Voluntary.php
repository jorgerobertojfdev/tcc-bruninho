<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voluntary extends Model
{
    protected $fillable = [
        'nome',
        'data_nascimento',
        'conjuge',
        'estado_civil',
        'filhos',
        'telefone',
        'celular',
        'companhia',
        'funcao',
        'data_reuniao',
        'quando_comeca',
        'horario_disponiveis'
    ];
    protected $dates = ['data_nascimento'];

    protected $casts = [
        'data_nascimento' => 'date:Y-m-d',
        'data_reuniao' => 'date:Y-m-d'
    ];

    public function address() {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function document() {
        return $this->morphOne('App\Document', 'documentable');
    }

    public function getDataReuniaoAttribute() {
        return $this->attributes['data_reuniao'];
    }
}
