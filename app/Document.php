<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'cpf',
        'identidade_numero',
        'identidade_uf',
        'identidade_orgao_expedidor',
        'identidade_data_emissao',
        'certidao_numero',
        'certidao_cartorio_livro',
        'certidao_folha',
        'certidao_termo',
        'certidao_matricula',
        'certidao_data_emissao'
    ];

    public function documentable()
    {
        return $this->morphTo();
    }
}
