<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $fillable = [
        'descricao',
        'destino',
        'data_entrada',
        'vencimento',
        'data_saida'
    ];
    protected $dates = [
        'data_entrada',
        'vencimento',
        'data_saida'
    ];
    protected $casts = [
        'data_nascimento' => 'date:Y-m-d',
        'vencimento' => 'date:Y-m-d',
        'data_saida' => 'date:Y-m-d'
    ];

}
